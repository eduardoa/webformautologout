More details about the module at [description docs](https://codimd.web.cern.ch/s/rkbADDxKU#)

Currently deployed locally on sites needing this, including:

- https://test-atlas-voting.web.cern.ch
- https://test-edu-voting.web.cern.ch


## Webform confidential submissions with invitation code auto-logout

A custom module to implement this functionality has been created.

![](https://codimd.web.cern.ch/uploads/upload_beac6167acdfdf3f86a534e3f846883e.png)

Once enabled, the module will autologout user when trying to respond to a webform if all the following conditions are met:

- The webform has the confidential option enabled (Settings -> Submissions -> Submission behaviors)

![](https://codimd.web.cern.ch/uploads/upload_b0df27815fb2bb32065e7f25058e7596.png)

- The user has a active session on the site (authenticated)
- The user is accessing the webform submission page while providing an invitation code. Example url: `https://YOURSITE.web.cern.ch/form/test?code=9bf434ffaa51e7d4e625ads89df89`

If all the conditions are met the user will be automatically logout by ending the user session to avoid the following screen and have a more clear workflow.

![](https://codimd.web.cern.ch/uploads/upload_3b77b0f14dc7a0466a5b05682a0482d3.png)
